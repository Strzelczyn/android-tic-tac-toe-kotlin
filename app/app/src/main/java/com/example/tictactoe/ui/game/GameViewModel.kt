package com.example.tictactoe.ui.game

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.tictactoe.R
import java.util.*

class GameViewModel : ViewModel() {

    val textButton00 = MutableLiveData<String>()

    val textButton01 = MutableLiveData<String>()

    val textButton02 = MutableLiveData<String>()

    val textButton10 = MutableLiveData<String>()

    val textButton11 = MutableLiveData<String>()

    val textButton12 = MutableLiveData<String>()

    val textButton20 = MutableLiveData<String>()

    val textButton21 = MutableLiveData<String>()

    val textButton22 = MutableLiveData<String>()

    val winMsg = MutableLiveData<String>()

    var sign = "X"

    val list = Array(9) { "" }

    var endGame = false

    fun Click(v: View) {
        if (endGame) {
            return
        }
        when (v.id) {
            R.id.button00 -> {
                list[0] = sign
                setMark(textButton00)
            }
            R.id.button01 -> {
                list[1] = sign
                setMark(textButton01)
            }
            R.id.button02 -> {
                list[2] = sign
                setMark(textButton02)
            }
            R.id.button10 -> {
                list[3] = sign
                setMark(textButton10)
            }
            R.id.button11 -> {
                list[4] = sign
                setMark(textButton11)
            }
            R.id.button12 -> {
                list[5] = sign
                setMark(textButton12)
            }
            R.id.button20 -> {
                list[6] = sign
                setMark(textButton20)
            }
            R.id.button21 -> {
                list[7] = sign
                setMark(textButton21)
            }
            R.id.button22 -> {
                list[8] = sign
                setMark(textButton22)
            }
        }
        checkWin()
    }

    private fun checkWin() {
        var signToCheck: String
        if (sign == "X") {
            signToCheck = "O"
        } else {
            signToCheck = "X"
        }
        for (i in 0..6 step 3) {
            if (list[i + 0] == signToCheck && list[i + 1] == signToCheck && list[i + 2] == signToCheck) {
                setEndGame(signToCheck)
            }
        }
        for (i in 0..2) {
            if (list[i + 0] == signToCheck && list[i + 3] == signToCheck && list[i + 6] == signToCheck) {
                setEndGame(signToCheck)
            }
        }
        if (list[0] == signToCheck && list[4] == signToCheck && list[8] == signToCheck) {
            setEndGame(signToCheck)
        }
        if (list[6] == signToCheck && list[4] == signToCheck && list[2] == signToCheck) {
            setEndGame(signToCheck)
        }
        if (!endGame && list.all { it != "" }) {
            endGame = true
            winMsg.value = "Draw"
        }
    }

    fun setEndGame(signToCheck: String) {
        endGame = true
        winMsg.value = "Game win player $signToCheck"
    }

    fun setMark(field: MutableLiveData<String>) {
        if (field.value.isNullOrEmpty()) {
            field.value = sign
            if (sign == "X") {
                sign = "O"
            } else {
                sign = "X"
            }
        }
    }

    fun reset() {
        endGame = false
        textButton00.value = ""
        textButton01.value = ""
        textButton02.value = ""
        textButton10.value = ""
        textButton11.value = ""
        textButton12.value = ""
        textButton20.value = ""
        textButton21.value = ""
        textButton22.value = ""
        winMsg.value = ""
        sign = "X"
        Arrays.fill(list, "")
    }
}
